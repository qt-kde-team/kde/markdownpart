markdownpart (24.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (24.12.0).
  * Update build-deps and deps with the info from cmake.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 24 Dec 2024 00:38:19 +0100

markdownpart (24.08.2-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update build dependencies and rules for KF6.
  * Update Standards-Version to 4.7.0, no changes needed.

 -- Simon Quigley <tsimonq2@debian.org>  Sat, 02 Nov 2024 13:40:29 -0500

markdownpart (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:58:05 +0100

markdownpart (22.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.1).
  * Bump Standards-Version to 4.6.2, no change required.
  * Add Albert Astals Cid’s master key to upstream signing keys.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 06 Jan 2023 23:41:02 +0100

markdownpart (22.12.0-2) unstable; urgency=medium

  * Upload with a correctly released changelog.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 16:05:02 +0100

markdownpart (22.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.08.3).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.1, no change required.
  * New upstream release (22.11.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (22.12.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 00:13:09 +0100

markdownpart (21.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (21.12.3).
  * Added myself to the uploaders.
  * Build with hardening=+all build hardening flag.
  * Refresh upstream metadata.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 07 Apr 2022 00:01:11 +0200

markdownpart (21.08.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the upstream GPG signing key.
  * Bump Standards-Version to 4.6.0, no changes required.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.16
    - bump Qt packages to 5.15.0
    - bump KF packages to 5.80.0

 -- Pino Toscano <pino@debian.org>  Sun, 22 Aug 2021 10:18:34 +0200

markdownpart (20.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Upstream uses the release-service, so switch the watch file, and the GPG
    signing key to it.
  * Bump Standards-Version to 4.5.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Wed, 16 Dec 2020 11:40:46 +0100

markdownpart (0.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Update the patches:
    - upstream_Lower-min-requirements-to-Qt-5.14-KF-5.66.patch: drop,
      backported from upstream
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Explicitly add the gettext build dependency.

 -- Pino Toscano <pino@debian.org>  Sun, 18 Oct 2020 23:39:05 +0200

markdownpart (0.1.0-1) unstable; urgency=medium

  * Initial packaging.

 -- Pino Toscano <pino@debian.org>  Sat, 29 Aug 2020 15:01:06 +0200
